Datagen Network
===================

This module enable a device as a /sensor/ for data generation.

Is designed for modeling and simulate a network of data generators (sensors).

As example the data is near a GNSS (geodesic) object of data, randomml
generated.

You can try the commands

```
datagen-serve
```

And 

```
datagen-client
```

Or, using cargo.

```
cargo run --bin datagen-server
```
On other terminal, even other point on the network.

```
cargo run --bin datagen-client
```

This commands has options that can be changed and checked with /help/.

Use as library
-----------------

To use in another crate.

```
cargo add datagen_network
```


Use as commands
------------------

Use the server or client commands.


```
cargo install datagen_network
```

Async While Pattern Used
-----------------------------

A rust implementation of this can be found on this example.

[clap and async test](https://crates.io/crates/clap-and-async-test)


TODO
====

- do more tests with sockets and streams of data.
- do all for generic data type
- saving data to file
- savin data to database
- actions from client 
- tracing remote subscription
