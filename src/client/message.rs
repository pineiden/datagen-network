use datagen_gnss::elements::data::Data;
use tokio::sync::broadcast::Sender;
use tracing::{error, info};


#[doc = r#"
This async function is connected with the function that manage the TcpStream
an receive the data as bytes by a broadcast channel.
Using a simple criteria with the end_flag agroup all slices received and
deserialize to create the structured d
ata. 
"#]
#[tracing::instrument(level = "info")]
pub async fn receive_and_save_data(
    tx_source: Sender<String>,
    end_flag: &String) {
    info!("Starting receive and save data for");
    let mut rx_source = tx_source.subscribe();
    let mut lines: Vec<String> = vec![];

    loop {
        match rx_source.recv().await {
            Ok(line) => {
                if line.as_str().trim() == end_flag {
                    let new_value = lines.join("");
                    let new_data = Data::json_loads(&new_value);
                    info!("Data converted to {}", new_data);
                    lines.clear();
                } else if !line.trim().is_empty(){
                    lines.push(line);
                };
            }
            Err(error) => {
                error!("Error on receive data, {}", error);
            }
        }
    }
}
