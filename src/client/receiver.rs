use super::super::server::uri::URI;
use std::time::Duration;
use tokio::io::BufReader;
use tokio::io::{AsyncBufReadExt};
use tokio::net::TcpStream;
use tokio::sync::broadcast::Sender;
use tokio::time::sleep;
use tracing::{error, info};

#[doc = r#"
This async function create and manage a client TcpStream connection.
In case of error or socket server fails, try to reconnect forever.
Just receive bytes and send by the broadcast channel. 
"#]
#[tracing::instrument(level = "info")]
pub async fn run_client(uri: &URI, tx_source: Sender<String>, end_flag: &String) {
    let addr = uri.addr().unwrap();
    loop {
        // connect part
        // this is a socket
        // let mut client_socket:TcpStream =
        match TcpStream::connect(&addr).await {
            Ok(mut client_socket) => {
                let local = client_socket.local_addr().unwrap();
                let remote = client_socket.peer_addr().unwrap();
                info!("Connected from {} to {}", local, remote);
                let tx_source = tx_source.clone();
                let (reader, mut  _writer) = client_socket.split();
                let mut bufreader = BufReader::new(reader);
                let mut line = String::new();
                loop {
                    // reader.readable().await.unwrap();
                    // match client_socket.readable().await {
                    //     Ok(yes) => {
                    match bufreader.read_line(&mut line).await {
                        Ok(0) => break,
                        Ok(_n) => {
                            tx_source.send(line.clone()).unwrap();
                            // accumulate to vec lines
                            // convert when line is *end*
                            line.clear();
                        }
                        Err(_) => {
                            info!("Error at reading from socket");
                            break;
                        }
                    }
                    //     },
                    //     Err(_) => {info!("Socket can't be readed")}
                    // }
                }
            }
            Err(err) => {
                error!("Error on try connection, {}", err);
                sleep(Duration::from_secs(5)).await;
            }
        };
    }
}
