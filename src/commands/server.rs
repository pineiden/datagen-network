extern crate clap;
extern crate datagen_network;
extern crate tokio;

use clap::{App, Arg};
use datagen_network::server::machine;
use datagen_network::scheme::Scheme;
use tracing::{span, Instrument, Level};
use tracing_subscriber;
mod validators;
use tracing::info;
use validators::isnumeric;
use clap::{Parser,ArgEnum};


// "This is the datagen server cli app"
#[derive(Parser,Debug)]
#[clap(author, version, about, long_about=None)]
struct ServerArgs {
	// Give a scheme: http/https
	#[clap(short, long, value_parser, arg_enum)]
	scheme:Scheme,
	// Give a hostname, url or ip
	#[clap(short, long, value_parser)]
	host:String,
	// Give a valid port number
	#[clap(short, long, value_parser, default_value_t = 5017)]
	port:u16,
	// Give a protocol. name
	#[clap(long, value_parser)]
	protocol:String,
	// Give a version for communitation protocol
	#[clap(long, value_parser)]
	protocol_version:String,
	#[clap(long, value_parser)]
	// Give a name for the equipment
	equipment_name:String,
	#[clap(long, value_parser)]	
	//"Give a serie number for this equipment
	equipment_serie:u32,
	// Give an end flag string
	#[clap(long, value_parser)]
	end_flag:String
}


#[doc = r#"
This command allows you to create a TCP Socket server (ip and port provided)
Can create and connect to many clients. Create random data structured by
datagen-gnss and send to them. The set of data separated by a sign like /*end*/.
So you have to detect or define what is the separator to manage correctly the
stream of data.
With the command /--help/ you can access to the options and default values.
```
datagen-server --help
```
For testing networks is recommended define a set of number of serie and othre
identificators to difference every source.
For only one source is enough with the default values.
"#]
#[tokio::main]
pub async fn main() {
    tracing_subscriber::fmt::init();
    let span_main = span!(Level::TRACE, "main");
    info!("Starting datagen network server");
    // obtener parámetros de la línea de comandos

    let matches = ServerArgs::parse();

    /* getting the values */

    // scheme
	info!("Port exposed: {}",&matches.port);

    machine::datagen_server(
        &matches.scheme.str(), 
		&matches.host, 
		&matches.port, 
		&matches.protocol, 
		&matches.protocol_version, 
		&matches.equipment_name, 
		&matches.equipment_serie, 
		&matches.end_flag,
    )
    .instrument(span_main)
    .await;
}
