#[macro_use]
extern crate clap;
extern crate datagen_network;

use clap::{App, Arg};
use datagen_network::client::machine;
use datagen_network::scheme::Scheme;
mod validators;
use tracing::info;
use tracing::{span, Instrument, Level};
use tracing_subscriber;
use validators::isnumeric;
use clap::{Parser};



// "This is the datagen server cli app"
#[derive(Parser,Debug)]
#[clap(author, version, about, long_about=None)]
struct ClientArgs {
	// Give a scheme: http/https
	#[clap(short, long, value_parser, arg_enum)]
	scheme:Scheme,
	// Give a hostname, url or ip
	#[clap(short, long, value_parser)]
	host:String,
	// Give a valid port number
	#[clap(short, long, value_parser, default_value_t = 5017)]
	port:u16,
	// Give an end flag string
	#[clap(long, value_parser)]
	end_flag:String
}


#[doc = r#"
This command allows you to connect to a TCP Socket server (ip and port provided)
Can receive any set of data separated by a sign like /*end*/.
So you have to detect or define what is the separator to manage correctly the
stream of data.
With the command /--help/ you can access to the options and default values.
```
datagen-client --help
```
"#]
#[tokio::main]
pub async fn main() {
    tracing_subscriber::fmt::init();
    let span_main = span!(Level::TRACE, "main");
    info!("Starting client for datagen server");
    // obtener parámetros de la línea de comandos

    let matches = ClientArgs::parse();

    machine::datagen_client(
		&matches.scheme.str(), 
		&matches.host, 
		&matches.port, 
		&matches.end_flag)
        .instrument(span_main)
        .await;
}
