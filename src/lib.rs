
pub mod client;
pub mod server;

pub mod scheme {
	use clap::{ArgEnum};
	use std::fmt;

	#[derive(ArgEnum, Clone, Copy, Debug)]
	pub enum Scheme {
		HTTP,
		HTTPS
	}

	impl fmt::Display for Scheme {
		fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
			match self {
				Scheme::HTTP => write!(f, "http"),
				Scheme::HTTPS => write!(f, "https"),
			}		
		}
	}

	impl Scheme {
		pub fn str(&self) -> String {
			format!("{}", self)
		} 
	}

}
