extern crate datagen_gnss;
use super::uri::URI;
use datagen_gnss::elements::data::Data;
use tokio::io::BufReader;
use tokio::io::{AsyncBufReadExt, AsyncWriteExt};
use tokio::net::TcpListener;
use tokio::sync::broadcast::Sender;
use tracing::info;


#[doc = r#"
This function create a TCP Socket and enables a listener that can allow clients
connections that receive data.
The data is received from another functions that generates a data with some
predefined frequency.
So the data is created, serialized and sended to all clients.
Requires  an URI object, the broadcast transmisors to enables channels.
And to finalize, define the *end_flag* string to separate the stream.
This pattern allows multiple client and broadcasting to all of them the data generated.
"#]
#[tracing::instrument(level = "info")]
pub async fn run_server(
    uri: &URI,
    tx_source: Sender<Data>,
    tx_client: Sender<Data>,
    end_flag: &String,
) {
    info!("Starting server at {}", uri);
    let addr = uri.addr().unwrap();
    info!("The socket address is {}", addr);
    let listener = TcpListener::bind(&addr).await.unwrap();
    loop {
        let (mut socket, addr) = listener.accept().await.unwrap();
        info!("A new client is connected from {}", addr);

        // clone sender stream for every client
        let _tx_action = tx_client.clone();
        let mut rx_action = tx_client.subscribe();
        // subscribe to msg sended by datagen
        let mut rx = tx_source.subscribe();
        let end_flag = end_flag.clone();

        tokio::spawn(async move {
            let (reader, mut writer) = socket.split();
            let mut reader = BufReader::new(reader);
            let mut line = String::new();
            loop {
                tokio::select! {
                      // receive from client action message
                      result = reader.read_line(&mut line) => {
                          if result.unwrap() == 0 {
                              break;
                          }
                          //tx_action.send((line.clone(),
                          // addr)).unwrap();
                          info!("Received from client {}",line);
                          line.clear();
                      }
                      // TODO :: receive result from action
                      result = rx_action.recv() => {
                let msg = result
                    .unwrap();
                          //if addr != other_addr {
                          writer.write_all(
                              "*action*".as_bytes())
                    .await
                    .unwrap();
                writer.write_all(msg.json_dumps().as_bytes())
                    .await
                    .unwrap();
                          writer.write_all(
                              "*action*".as_bytes())
                    .await
                    .unwrap();
                      }

                      // receive from source and send to socket
                      result = rx.recv() => {
                          let msg = result.unwrap();
                          //if addr != other_addr {
                          writer.write_all(
                              msg
                                  .json_dumps()
                                  .as_bytes())
                              .await
                              .unwrap();
                          writer.write_all(
                             "\n"
                                  .to_string()
                                  .as_bytes())
                              .await
                              .unwrap();
                          writer.write_all(
                              end_flag
                                  .to_string()
                                  .as_bytes())
                              .await
                              .unwrap();
                          writer.write_all(
                              "\n"
                                  .to_string()
                                  .as_bytes())
                              .await
                              .unwrap();
                      }
                  }
            }
        });
    }
}
