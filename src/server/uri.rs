use serde::{Deserialize, Serialize};
use std::fmt;
use std::net::SocketAddr;
use tracing::{error, info};

#[doc = r#"
This struct manage the values to build a complete URL and create then a
/SocketAddress/, for example.
The main values are /host/ and /port/, a secondary is /scheme/ that could be
used in the future for http or websocket or other similar.
For the moment /scheme/ field can be omitted.
"#]
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct URI {
    pub scheme: String,
    pub host: String,
    pub port: u16,
}

impl fmt::Display for URI {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}:{}", self.host, self.port)
    }
}

impl URI {
    pub fn new(scheme: String, host: String, port: u16) -> URI {
        URI { scheme, host, port }
    }

    pub fn default(&self) -> URI {
        URI::new("http".to_string(), "localhost".to_string(), 8080)
    }

    pub fn json_dumps(&self) -> String {
        serde_json::to_string_pretty(&self).unwrap()
    }

    pub fn json_loads(serialized: &str) -> Self {
        serde_json::from_str(&serialized).unwrap()
    }

    pub fn addr(&self) -> Result<SocketAddr, std::string::ParseError> {
        let slice = &self.to_string();
        match slice.parse::<SocketAddr>() {
            Ok(val) => {
                info!("El valor de address {}", val);
                Ok(val)
            }
            Err(err) => {
                error!("El valor de error al parsear {}", err);
                panic!("Error on parsing URI to socket addr")
            }
        }
    }
}
