use super::listener;
use super::message;
use super::uri::URI;
use datagen_gnss::elements::equipment::Equipment;
use futures::future::{self};
use tokio::sync::broadcast::{self};
use tracing::{info, span, Instrument, Level};

#[doc = r#"
Create the instance for a socket server(TcpListener) connected with
the data generator.
Uses data argument from cli to create the parameters.

Server Parameters:
- scheme: http/https
- host: localhost/ip
- port: valid port number

Station Parameters:
- protocol: string name of the communitation protocol
- version: string, code version of protocol
- equipment: {name, serie_id}

All the paramters must be simple string or numbers, so on the main
function (using clap mayb or args parsing) you can obtain or define
the values.
"#]
#[tracing::instrument(level = "info")]
pub async fn datagen_server(
    scheme: &String,
    host: &String,
    port: &u16,
    protocol: &String,
    version: &String,
    equipment_name: &String,
    equipment_serie: &u32,
    end_flag: &String,
) {
    let equipment = Equipment::new(equipment_name.to_string(), *equipment_serie);
    // create the uri.
    let uri = URI::new(scheme.to_string(), host.to_string(), *port);
    let span_data = span!(Level::TRACE, "DATA");
    let span_serve = span!(Level::TRACE, "SERVE");

    info!("Starting datagen_server");
    info!("URI defined by {}", uri);
    // create the channels to communicate among the threads.
    // the important object is tx so the threads can subscribe to the channel
    let (tx_client, _rx) = broadcast::channel(100);
    let (tx_source, _rx) = broadcast::channel(100);

    // run the serer
    let gen_tx_client = tx_client.clone();
    let gen_tx_source = tx_source.clone();
    let new_version = version.clone();
    let new_protocol = protocol.clone();
    let new_end_flag = end_flag.clone();
    let data_task = tokio::spawn(async move {
        message::create_and_send_data(
            &new_protocol,
            &new_version,
            &equipment,
            gen_tx_source,
            gen_tx_client,
        )
        .instrument(span_data)
        .await;
    });
    // run the data generator
    let server_tx_client = tx_client.clone();
    let server_tx_source = tx_source.clone();
    let server_task = tokio::spawn(async move {
        listener::run_server(&uri, server_tx_source, server_tx_client, &new_end_flag)
            .instrument(span_serve)
            .await;
    });
    // the 'main code to spawn async fn'
    //Ok(())
    let v = vec![data_task, server_task];
    let _outputs = future::try_join_all(v).await;
}
