extern crate datagen_gnss;

use datagen_gnss::elements::{data::Data, equipment::Equipment};
use datagen_gnss::generators::create_data;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::time::Duration;
use tokio::sync::broadcast::Sender;
use tokio::time::{sleep_until, Instant};
use tracing::{error, info};

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Action {
    pub action: String,
    pub value: u32,
    pub result: String,
}

impl Action {
    pub fn new(action: String, value: u32) -> Action {
        Action {
            action,
            value,
            result: String::new(),
        }
    }

    pub fn json_dumps(&self) -> String {
        serde_json::to_string_pretty(&self).unwrap()
    }

    pub fn json_loads(serialized: &str) -> Self {
        serde_json::from_str(&serialized).unwrap()
    }
}

impl fmt::Display for Action {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Action(action: {}, value: {}, result: {})",
            self.action, self.value, self.result
        )
    }
}

#[doc = r#"
This function create a new random data structured by datagen-gnss and send by a
broadcast channel to other function that receive and send by socket.
Need referece for protocol, version, equipment and the Sender objects.
"#]
#[tracing::instrument(level = "info")]
pub async fn create_and_send_data(
    protocol: &String,
    version: &String,
    equipment: &Equipment,
    tx_source: Sender<Data>,
    tx_client: Sender<Data>,
) {
    info!(
        "Starting create and send data for {} : {}",
        protocol, version
    );
    // receive from socket
    let mut _rx_client = tx_client.subscribe();
    let time_sleep = 1;
    // loop {
    // 	tokio::spawn(async move {
    // 		result = rx_client.recv() => {
    // 			let (msg, addr) = result.unwrap();
    // 			// deserialize and do action
    // 			// result
    // 			// send result to tx
    // 			// tx_client.send(msg).await.unwrap();
    // 			let mut message: Action = Action::json_loads(&msg);

    // 			match message.action {
    // 				&"sleep" => {
    // 					time_sleep = message.value;
    // 					message.result = &"cambio de valor realizado
    // 				correctamente";2
    // 					info!(message.result, time_sleep);
    // 					tx_client.send(message.json_dumps()).await.unwrap();
    // 				},
    // 				_ => {
    // 					// todo:: loggin tracing
    // 					warn!("AccciÃ³n no reconocida {}", message)
    // 				}
    // 			}
    // 		}

    // 	});
    // data generator base, ok
    loop {
        let until = Instant::now() + Duration::from_secs(time_sleep);
        let data = create_data(protocol, version, equipment);
        // put on io/reader
        info!("Sending data to tx_source {}", data);
        if tx_source.receiver_count() > 0 {
            match tx_source.send(data) {
                Ok(value) => info!("Sended value OK {}", value),
                Err(err) => {
                    error!("Error on send value {}", err);
                    panic!("Error on send value {}", err)
                }
            };
        }
        sleep_until(until).await;
    }
}
